const { src, dest, watch, parallel, series } = require('gulp')
const gulpPug = require('gulp-pug')
const mjml = require('gulp-mjml')
const rename = require('gulp-rename')
const cleaner = require('del')
const browser = require('browser-sync')

// servers
function startServer (done) {
  browser.init({
    port: 9090,
    server: {
      baseDir: 'public',
      directory: true
    }
  })

  done()
}

function reloadServer (done) {
  browser.reload()
  done()
}

// dir cleanup
function cleanup () {
  return cleaner(['public', 'mjml'], { read: false })
}

// builders: pug to html
function buildHomepage () {
  return src('src/**/!(*.mjml).pug')
    .pipe(gulpPug({ pretty: true }))
    .pipe(dest('public'))
}

// builders: pug to mjml
function buildEdm () {
  return src('src/**/*.mjml.pug')
    .pipe(gulpPug())
    .pipe(rename(filepath => {
      filepath.extname = ''
    }))
    .pipe(dest('mjml'))
}

// builders: mjml to html
function buildMjml () {
  return src('mjml/**/*.mjml')
    .pipe(mjml())
    .pipe(dest('public'))
}

// builders: file duplicators
function copyAssets () {
  return src(['src/**/*.css', 'src/**/*.js', 'src/**/*.jpg', 'src/**/*.png'])
    .pipe(dest('public'))
}


// watchers:
function watchHomepage () {
  return watch('src/homepage/*.pug')
    .on('change', series(buildHomepage, reloadServer))
}

function watchEdm () {
  return watch('src/edm/*.pug')
    .on('change', series(buildEdm, buildMjml, reloadServer))
}

function watchAssets () {
  return watch(['src/**/*.css', 'src/**/*.js', 'src/**/*.jpg', 'src/**/*.png'])
    .on('change', series(copyAssets, reloadServer))
}


exports.default = series(cleanup, startServer, buildHomepage, buildEdm, buildMjml, copyAssets, parallel(watchHomepage, watchEdm, watchAssets))
exports.build = series(cleanup, buildHomepage, buildEdm, buildMjml, copyAssets)
