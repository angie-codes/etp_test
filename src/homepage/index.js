const heroOne = $('li#one')
const heroTwo = $('li#two')
const heroTitle = $('#hero-title')
const heroDesc = $('#hero-desc')

const cardContainer = $('.card-container')
let currentPosition = parseInt(cardContainer.css('transform').split(',')[4])
const sliderLeft = $('#left')
const sliderRight = $('#right')

heroOne.click(event => {
  if (heroOne.hasClass('active') === false) {
    heroOne.addClass('active')
    heroTwo.removeClass('active')

    heroTitle.html('Prow scuttle parrel provost Sail<br>lookout wherry doubloon chase')
    heroDesc.text('Case shot Shiver me timbers gangplank')
  }
})

heroTwo.click(event => {
  if (heroTwo.hasClass('active') === false) {
    heroTwo.addClass('active')
    heroOne.removeClass('active')

    heroTitle.html('Parrots are omnivores')
    heroDesc.text('Save the forests!')
  }
})


sliderLeft.click(e => {
  currentPosition = currentPosition + 300
  if (currentPosition > 0) {
    currentPosition = 0
  }
  // console.log('right button clicked @ ', currentPosition)
  cardContainer.css('transform', `translateX(${currentPosition}px)`)
})

sliderRight.click(e => {
  currentPosition = currentPosition - 300
  if (currentPosition < -1200) {
    currentPosition = 0
  }
  // console.log('right button clicked @ ', currentPosition)
  cardContainer.css('transform', `translateX(${currentPosition}px)`)
})
