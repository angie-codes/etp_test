# Test submission for review purposes

### Objective:
1. To build and deploy one responsive homepage (choose one of two options)
2. To build and blast one responsive EDM

### Links:

1. [Homepage](https://entropia.lckd.pw/homepage/)
2. [EDM](https://entropia.lckd.pw/edm/)
